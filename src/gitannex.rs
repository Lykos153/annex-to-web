//! Helper functions for git-annex
//!
//! These encode some knowledge of git-annex internals that are hopefully stable, and sometimes
//! save us the trouble of calling git-annex externally (e.g. for examinekey).

use serde::Deserialize;

pub const WEB_UUID: &str = "00000000-0000-0000-0000-000000000001";
pub const TORRENT_UUID: &str = "00000000-0000-0000-0000-000000000002";

/// If a given symlink blob content is a git-annex symlink (recognized by any number of "../"
/// followed by ".git/annex/objects/"), its path relative to the repository (guaranteed not to go
/// up) and object name will be returned.
///
/// The number of "../" is not checked; further structure may or may not be checked (but whoever
/// can craft a malformed annex symlink could just as well craft a valid one in the same place).
///
/// This returns str / OsStr because git-annex never has non-utf8 bytes in its file names (as
/// checked by trying to bring a file "test.ex\ff" into a SHA256E annex -- the extension is just
/// ignored in the key); confirmation on this is being requested at
/// <https://git-annex.branchable.com/internals/key_format/#comment-a8b45879643a7e60cb36abec28bc0646>.
pub fn is_annex_symlink(link: &[u8]) -> Option<(&str, &str)> {
    let mut tail = std::str::from_utf8(link).ok()?;
    while tail.starts_with("../") {
        tail = &tail[3..];
    }
    if !tail.starts_with(".git/annex/objects/") {
        return None;
    }
    let relative = tail;

    // ".git/annex/objects/" + 2 groups of "xx/" + one slash inbetween + at least one char of
    // actual key
    if tail.len() < 19 + 6 + 1 + 2 {
        return None;
    }

    tail = &tail[19 + 6..];
    let slash = tail.chars().position(|x| x == '/')?;
    let key = &tail[..slash];
    let key2 = &tail[slash + 1..];

    if key == key2 {
        Some((relative, key))
    } else {
        None
    }
}

/// Given a key, calculate the "new hash" (also called "hashdirlower"), consisting of six hex digits
/// in two groups of three, each group terminated by a slash.
///
/// This is useful in finding the actually stored path in a bare git-annex repository.
///
/// Usualy, this would be determined by `git annex examinekey --format '$(hashdirlower}' $KEY`, but that's a lot of forking
/// for a simle calculation.
pub fn hashdirlower(key: &str) -> String {
    use md5::Digest;
    let mut hash = md5::Md5::new();
    hash.input(key.as_bytes());
    let bin = hash.result();
    // FIXME: I'd like to do this more efficiently but also not pull in a crate for hex-formatting
    let hexencoded = format!("{:x}{:x}{:x}", bin[0], bin[1], bin[2]);
    let (first, second) = hexencoded.split_at(3);
    return format!("{}/{}/", first, second);

    // FIXME I'd rather return a [u8; 8] that asserts it's ASCII
}

#[test]
fn test_hashdirlower() {
    let key = "SHA256E-s111149056--12ef7ef11e9613754bf03497366be791efb183cad30e5bfaa2fcdcf3b5779e24.txt";
    let hdl = hashdirlower(key);
    assert_eq!(&hdl, "d05/8a1/");
}

const HASHDIRMIXED_SYMBOLS: &[u8] = b"0123456789zqjxkmvwgpfZQJXKMVWGPF";

/// Given a key, calculate the "old hash" (also called "hashdirmixed"), consisting of four
/// characters in two groups of two, each group terminated by a slash.
///
/// This is useful in finding the actually stored path in a non-bare git-annex repository.
///
/// Usualy, this would be determined by `git annex examinekey --format '$(hashdirmixed}' $KEY`, but that's a lot of forking
/// for a simle calculation.
pub fn hashdirmixed(key: &str) -> String {
    use md5::Digest;
    let mut hash = md5::Md5::new();
    hash.input(key.as_bytes());
    let bin = hash.result();
    // digits in the hash bytes:
    // 11_2 2222  4444 _111  _333 33_4  ____ ____
    // result after mapping in
    // 12/34/
    let letters = [
        (bin[0] >> 6) | ((bin[1] & 0x7) << 2),
        bin[0] & 0x1f,
        (bin[2] >> 2) & 0x1f,
        ((bin[2] & 1) << 4) | (bin[1] >> 4),
    ];
    let mut letters = letters.iter().map(|i| HASHDIRMIXED_SYMBOLS[*i as usize] as char);

    return format!("{}{}/{}{}/",
                   letters.next().unwrap(),
                   letters.next().unwrap(),
                   letters.next().unwrap(),
                   letters.next().unwrap(),
                   );

    // FIXME I'd rather return a [u8; 6] that asserts it's ASCII
}

#[test]
fn test_hashdirmixed() {
    let key = "SHA256E-s111149056--12ef7ef11e9613754bf03497366be791efb183cad30e5bfaa2fcdcf3b5779e24.txt";
    let hdm = hashdirmixed(key);
    assert_eq!(&hdm, "3v/8Z/");
}

use std::path::Path;

pub fn hashpath(repopath: &Path, is_bare: bool, key: &str) -> std::path::PathBuf {
    if is_bare {
        let hashprefix = hashdirlower(key);
        repopath.join("annex").join("objects").join(&hashprefix[..]).join(key).join(key)
    } else {
        let hashprefix = hashdirmixed(key);
        repopath.join(".git").join("annex").join("objects").join(&hashprefix[..]).join(key).join(key)
    }
}

#[derive(Deserialize)]
struct WhereisResponse {
    whereis: Vec<WhereisItem>,
}

// All this would be doable with &'a str into the parse result, but then the caller would need
// to do this two-staged and get an owned string out first and then have that parsed to refs.
#[derive(Deserialize, Debug)]
pub struct WhereisItem {
    pub description: String,
    pub urls: Vec<String>,
    pub uuid: String,
}

/// Run `git annex whereis` in a repository at `repopath` looking for a given `key`.
///
/// This adds, as a workaround as described in <https://git-annex.branchable.com/bugs/creating_dot-git-as-symlink_workaround_drops_worktree_configuration_from_submodules/>,
/// a GIT_WORK_TREE=repopath to mollfiy git-annex when called in a submodule that has its
/// core.worktree restored to what it should be like in a submodule; that does not appear to have
/// any ill-effects in non-bare repositories.
// FIXME denoted async anticipating re-implementation once https://github.com/async-rs/async-std/issues/22 is done
pub async fn whereis(repopath: &std::path::Path, key: &str) -> Result<Vec<WhereisItem>, ()> {
    let cmd = std::process::Command::new("git-annex")
        .current_dir(repopath)
        .env("GIT_WORK_TREE", repopath)
        .arg("whereis").arg("--json").arg("--key").arg(key)
        .output()
        .map_err(|_| ())?;
    if !cmd.status.success() {
        return Err(());
    }
    let response = std::str::from_utf8(&cmd.stdout)
        // It's JSON, non-UTF8 output here would mean a broken git-annex
        .map_err(|_| ())?;

    let parsed: WhereisResponse = serde_json::from_str(response)
        .map_err(|_| ())?;

    Ok(parsed.whereis)
}
