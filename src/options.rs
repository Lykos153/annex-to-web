/// Application options as exposed on the command line

use structopt::StructOpt;

// The configuration parts relevant to the the server or the handler
//
// For simplicity, they are all rolled into a single struct -- the `bind` data is needlessly
// passed into the handlers, but that does no great harm.
//
// Literals in here are owned heap allocations to avoid the lifetime mess of passing them through
// make_svc & co.
/// Run a web server that serves a (possibly bare) git repository, and serves git-annex files on
/// demand, pulling them in or redirecting to other servers where the data is.
#[derive(Clone, Debug, StructOpt)]
pub struct ApplicationConfiguration {
    /// IP socket address to bind to. Example values are `[::]:8000` or `127.0.0.1:3000`.
    pub bind: std::net::SocketAddr,
    /// Git repository to serve under the root path
    pub repopath: std::path::PathBuf,
    /// Do not attempt to run `git annex get` on absent files and serve requests for them as they come in
    #[structopt(long="--no-get", parse(from_flag=negate))]
    pub get: bool,
    /// Pairs of UUIDs and corresponding base URIs. When files are not present and unobtainable,
    /// the user will be redirected to a location where the file is, with decreasing prefernence
    /// indicated by the order of command line arguments.
    ///
    /// If redirect is disabled, those are used to serve hyperlinks on the "not available locally"
    /// error page.
    ///
    /// Example: --redirect-base add3536d-a60a-4532-be54-e622efe3caf8:http://bologna.annex.example.com
    #[structopt(long="redirect-base", parse(try_from_str=colonsplit))]
    pub redirect_bases: Vec<(String, String)>,
}

fn negate(a: bool) -> bool {
    !a
}

fn colonsplit(input: &str) -> Result<(String, String), &str> {
    let mut iter = input.splitn(2, ':');
    Ok((
        iter.next()
            .expect("splitn returns at least one item")
            .to_string(),
        iter.next()
            .ok_or("Pairs must be split by a colon (':') character")?
            .to_string(),
    ))
}
