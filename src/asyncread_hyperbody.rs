use core::task::{Poll, Context};
use core::pin::Pin;

use futures::io::AsyncRead;

// Should not have a small remainder when divided by common TCP segemnt sizes, as Nagle seems not
// to kick in here reliably (a value of 100 on a WiFi network shows several 100byte or 300byte
// messages). This is most easily achieved with a large value, which on the other hand causes
// overhead on every unsuccessful poll, and may not be filled up completely anyway.
const BUFSIZE: usize = 4096;

pub struct AsyncReadHyperBody<F: AsyncRead> {
    file: F,
}

impl<F: AsyncRead> AsyncReadHyperBody<F> {
    pub fn new(file: F) -> Self {
        Self { file }
    }

    fn get_file_mut(self: Pin<&mut Self>) -> Pin<&mut F> {
        // This is okay because `field` is pinned when `self` is.
        // (Implementing the common structural pinning pattern)
        unsafe { self.map_unchecked_mut(|s| &mut s.file) }
    }
}

// This is a very crude implementation that does not attempt much optimization that might be
// possible by having a double buffer of which one part always reads from the file and the other
// pushes to the stream
impl<F: AsyncRead> hyper::body::HttpBody for AsyncReadHyperBody<F> {
    type Data = hyper::body::Bytes;
    type Error = hyper::Error;

    fn poll_data(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
    ) -> Poll<Option<Result<Self::Data, Self::Error>>> {
        // FIXME: Can we trust the compiler to optimize this right into a write to a
        // to-be-allocated Buf? Probably not, thus clearing out BUFSIZE of memory on each poll --
        // anyway, still good enough for now.
        let mut buf = [0u8; BUFSIZE];
        let file = self.get_file_mut();
        match file.poll_read(cx, &mut buf) {
            // FIXME No clue whether this error handling is correct
            Poll::Pending => Poll::Pending,
            // FIXME could reduce copying by reading right into Bytes
            Poll::Ready(Ok(n)) => Poll::Ready(Some(Ok(hyper::body::Bytes::copy_from_slice(&buf[..n])))),
            Poll::Ready(Err(_)) => Poll::Ready(None),
        }
    }

    fn poll_trailers(
        self: Pin<&mut Self>,
        _cx: &mut Context<'_>,
    ) -> Poll<Result<Option<hyper::HeaderMap>, Self::Error>> {
        Poll::Ready(Ok(None))
    }
}
