use std::pin::Pin;
use std::convert::TryInto;
use {
    hyper::{
        // Miscellaneous types from Hyper for working with HTTP.
        Body, Error, Request, Response, Server, StatusCode,

        // This function turns a closure which returns a future into an
        // implementation of the the Hyper `Service` trait, which is an
        // asynchronous function from a generic `Request` to a `Response`.
        service::{make_service_fn, service_fn},
    },
    std::net::SocketAddr,
};

use tokio::runtime::Runtime;
use git2::Repository;
use horrorshow::html;

mod gitannex;
mod asyncread_hyperbody;
mod options;

use options::ApplicationConfiguration;

/// The information for processing the later entity gatherable from a TreeEntry's file mode field
enum FileModeHint {
    File, // no matter whether executable or not
    Directory,
    Symlink,
    Gitlink
}

impl core::convert::TryFrom<&git2::TreeEntry<'_>> for FileModeHint {
    type Error = ();

    fn try_from(source: &git2::TreeEntry) -> Result<Self, ()> {
        match source.filemode() {
            0o120000 => Ok(FileModeHint::Symlink),
            0o160000 => Ok(FileModeHint::Gitlink),
            0o040000 => Ok(FileModeHint::Directory),
            0o100644 | 0o100755 => Ok(FileModeHint::File),
            _ => Err(()),
        }
    }
}

fn entry_to_href_and_name(t: git2::TreeEntry<'_>) -> (Option<String>, String) {
    let mode: Option<FileModeHint> = (&t).try_into().ok();

    // we already need to allocate for a string here as percent_encode doesn't
    // impl Render...
    let mut link = percent_encoding::percent_encode(t.name_bytes(), percent_encoding::NON_ALPHANUMERIC).collect::<String>();
    // so we can append right away as well
    let link = match (mode, t.kind()) {
        (Some(FileModeHint::Gitlink), _) |
        (_, Some(git2::ObjectType::Tree)) => {
            link.push('/');
            Some(link)
        },
        (_, Some(git2::ObjectType::Blob)) => {
            // Actually, on symlinks, we should look in and determine what it is -- or at least add
            // a slash if the link ends with a slash, as a short-cut and initial workaround
            Some(link)
        }
        _ => None
    };

    let label = String::from_utf8_lossy(t.name_bytes());
    // If we didn't need to convert anyway to insert any replacement characters, I'm sure there is
    // a way to make the iterator hand out &TreeEntry rather than TreeEntry (so that references
    // inside can be returned that are valid until the next invocation), but right now this is a
    // fast solution.
    let label = label.to_string();

    (link, label)
}

fn html_response(status: hyper::StatusCode, r: impl std::fmt::Display) -> Response<Pin<Box<dyn hyper::body::HttpBody<Data=hyper::body::Bytes, Error=hyper::Error> + Send>>> {
    let body: Body = format!("{}", r).into();
    let boxed: Pin<Box<dyn hyper::body::HttpBody<Data=hyper::body::Bytes, Error=hyper::Error> + Send>> = Box::pin(body);
    Response::builder()
        .status(status)
        .header(hyper::header::CONTENT_TYPE, "text/html;charset=utf-8")
        .body(boxed).unwrap()
}


// PartialEq: Just for asserts on results
#[derive(PartialEq)]
enum AppError {
    NotFound,
    DenormalizedPath,
    IncompleteGit(&'static str),
    NotImplemented(&'static str),
}

async fn serve_req_errormapping(req: Request<Body>, config: ApplicationConfiguration) -> Result<Response<Pin<Box<dyn hyper::body::HttpBody<Data=hyper::body::Bytes, Error=hyper::Error> + Send>>>, hyper::Error> {
    // If we are strict about the distinction, we can at least provide good errors
    let is_dir = req.uri().path().ends_with('/');

    Ok(serve_req(req, config).await.unwrap_or_else(|e| {
        let (status, body): (_, Body) = match e {
            // Could as well provide a link back
            AppError::NotFound => (StatusCode::NOT_FOUND, (if is_dir { "No such directory" } else { "No such file" }).into()),
            AppError::DenormalizedPath => (StatusCode::BAD_REQUEST, "Please normalize your paths before requesting".into()),
            AppError::IncompleteGit(s) => (StatusCode::INTERNAL_SERVER_ERROR, format!("Broken repository: {}", s).into()),
            AppError::NotImplemented(s) => (StatusCode::INTERNAL_SERVER_ERROR, format!("Implementation limit: {}", s).into()),
        };

        let boxed: Pin<Box<dyn hyper::body::HttpBody<Data=hyper::body::Bytes, Error=hyper::Error> + Send>> = Box::pin(body);
        Response::builder().status(status).body(boxed).unwrap()
    }))
}

struct Cursor {
    /// Indicates the repository currently traversed
    repo: Repository,
    /// Indicates the tree or blob currently focused at
    oid: git2::Oid,
    /// Indicate whether the OID points to a blob that is to be treated as a symlink
    mode: FileModeHint,
}

/// Provide an HTTP response to a request for a given hash (git-annex key) in the git-annex
/// repository that is indicated in the config.
///
/// repopath_is_bare is the only additional information needed on the repository (as it decides the
/// access method). In general this would probably work with git annex object stores outside a git
/// repository (think rsync remote) just as well; repopath_is_bare would be set to True then.
///
/// relative being passed in is merely an optimization -- it's the ".git/annex/objects/XX/YY/HH/HH"
/// part of the symlink from which the hash was usually obtained.
///
/// The request is only used for its URI, which should be better managed when the URI templating
/// involved in redirect bases is understood better.
async fn serve_annexed(req: Request<Body>, config: ApplicationConfiguration, repopath: &std::path::Path, repopath_is_bare: bool, relative: &str, hash: &str) -> Result<Response<Pin<Box<dyn hyper::body::HttpBody<Data=hyper::body::Bytes, Error=hyper::Error> + Send>>>, AppError> {
    let localpath;

    if repopath_is_bare {
        localpath = gitannex::hashpath(repopath, true, hash);
    } else {
        localpath = repopath.join(relative);
        debug_assert!(localpath == gitannex::hashpath(repopath, repopath_is_bare, hash));
    }

    let available = async_std::fs::File::open(localpath).await;

    if let Ok(file) = available {
        let length = file.metadata().await
            .map_err(|_| AppError::IncompleteGit("Failed to stat existing file"))?
            .len();
        let boxed: Pin<Box<dyn hyper::body::HttpBody<Data=hyper::body::Bytes, Error=hyper::Error> + Send>> = Box::pin(asyncread_hyperbody::AsyncReadHyperBody::new(file));
        return Ok(Response::builder()
            .header(hyper::header::CONTENT_LENGTH, format!("{}", length))
            .body(boxed)
            .unwrap());
    } else {
        let mut whereis = gitannex::whereis(repopath, hash)
            .await
            .map_err(|_| AppError::IncompleteGit("Error invoking git-annex"))?;

        #[derive(Debug, PartialOrd, Ord, PartialEq, Eq)]
        struct WebWhereisItem {
            detail: WebWhereisItemDetail,
            // careful: sequence matters for Ord
            description: String,
        }

        impl WebWhereisItem {
            fn format(&self, tmpl: &mut horrorshow::TemplateBuffer) {
                match &self.detail {
                    WebWhereisItemDetail::NotFound => tmpl << html! {
                        dt { : &self.description }
                    },
                    WebWhereisItemDetail::Web(urls) => tmpl << html! {
                        dt { : "On the web" }
                        @for url in urls.iter() {
                            dd { a(href=url) { : url } }
                        }
                    },
                    WebWhereisItemDetail::Torrent(urls) => tmpl << html! {
                        dt { : "Using BitTorrent" }
                        @for url in urls.iter() {
                            dd { a(href=url) { : url } }
                        }
                    },
                    WebWhereisItemDetail::HasServer(url) => tmpl << html! {
                        dt { : &self.description }
                        dd {
                            a(href=url) { : url }
                        }
                    },
                }
            }
        }

        #[derive(Debug, PartialOrd, Ord, PartialEq, Eq)]
        enum WebWhereisItemDetail {
            HasServer(String),
            Web(Vec<String>),
            Torrent(Vec<String>),
            NotFound,
        }

        impl WebWhereisItemDetail {
            fn with(self, description: String) -> WebWhereisItem {
                WebWhereisItem {
                    description,
                    detail: self
                }
            }
        }

        let mut whereis: Vec<_> = whereis
            // preferably something ...(self) -> impl Iterator<Item = T>
            .drain(..)
            .map(|x| {
                if x.uuid == gitannex::WEB_UUID {
                    WebWhereisItemDetail::Web(x.urls)
                } else if x.uuid == gitannex::TORRENT_UUID {
                    WebWhereisItemDetail::Torrent(x.urls)
                } else if let Some((_, base)) = config.redirect_bases.iter().find(|(u, _)| u == &x.uuid) {
                    // Treating them more like URI templates
                    WebWhereisItemDetail::HasServer(format!("{}{}", base, req.uri().path()))
                } else {
                    WebWhereisItemDetail::NotFound
                }.with(x.description)
            })
            // This makes the HTML's Fn type easier
            .collect();
        whereis.sort();

        return Ok(html_response(hyper::StatusCode::MULTIPLE_CHOICES, html! {
            : horrorshow::helper::doctype::HTML;
            html {
                head {
                    title : "File not found locally";
                }
                body {
                    p {
                        : "This file is not available locally; you may find it in any of those repositories:";
                    }
                    ul {
                        @for r in whereis.iter() {
                            |tmpl| r.format(tmpl)
                        }
                    }
                }
            }
        }));
        // FIXME provide a magnet link to load from *all* web sources?
    }
}

/// Wrapper for Repository::open that passes the typical parameetes for this application, and wraps
/// the error type.
fn open_repo(repopath: &std::path::Path) -> Result<Repository, AppError> {
    // No ceiling needed as no search enabled, but open_ext needs typed input anyway.
    let no_ceiling: &[&str] = &[];
    Repository::open_ext(
            repopath,
            // NO_SEARCH: Pointing to somewhere inside a repository would fail as the trailing path
            // information would be discarded, so it doesn't help, and could surprising (in a security
            // relevant way) if no git repo is present where expected.
            git2::RepositoryOpenFlags::NO_SEARCH,
            no_ceiling)
        .map_err(|_| AppError::IncompleteGit("Error opening repository"))
}

/// Given a path as produced by the .uri().path() of a Hyper request (the path component of a URI,
/// starting with a slash), produce an iterator over the percent-decoded path segments.
///
/// The segments are decoded into UTF-8 strings, and checked for following syntax-based
/// normalization (ie. not containing empty interior segments or dot segments).
///
/// The presence of a trailing slash (which is the only normalized way to get an empty segment) is
/// indicated by the boolean result.
fn request_path_to_segments(mut path: &str) -> (impl Iterator<Item=Result<std::borrow::Cow<'_, str>, AppError>>, bool)
{
    // Prelude to path-normalization: Chopping off the trailing slash into
    // a boolean makes the later checks for whether the path is normalized much easier.
    let path_trailing_slash;
    {
        let mut splitlastslash = path.rsplitn(2, '/');
        if splitlastslash.next() == Some("") {
            path = splitlastslash.next().expect("There is at least one slash in a hyper path");
            path_trailing_slash = true;
        } else {
            path_trailing_slash = false;
        }
    }

    // Not turning this into a std::path::Path becaus that'd introduce all the trouble of OsPath,
    // and between a bare git repo and a URI there need not be such confusion -- but decomposing
    // the URI's percent encoding makes the names more straight-forward to handle in an "arbitrary
    // strings as path components" setup that git largely is. (".", ".." and "" not being
    // permissible is a components is a limitation of URIs, not git).
    let mut path = path
        .split('/')
        .map(|component|
            percent_encoding::percent_decode_str(component)
            .decode_utf8()
            // Given we'd fail to use a [u8] further down, we'll use str right away
            //
            // In other places (esp. at the rendering side), those are correctly handled to the
            // extent possible.
            .map_err(|_| AppError::NotImplemented("git2 does not implement get_name for non-UTF8 filenames"))
        );

    // Not explicitly documented in hyper, but practically required by the underlying protocol;
    // stripping off that leading slash.
    //
    // This best happens here after the split (as it consumes the one iteration element split
    // always produces) but before normalization (because the first element is indeed empty).
    let empty = path.next();
    assert!(empty == Some(Ok(std::borrow::Cow::Borrowed(""))));

    // Ensure path is syntax-normalized. This is primarily a precaution -- those paths' wouldn't be
    // practically usable (as browsers do syntax normalization before they GET), and if any
    // downstream component happens to be added later that does not walk the tree itself but passes
    // the opaque identifiers to something that'd put meaning to them (eg. the file system),
    // potential security issues are caught early here.
    let path = path
        .map(|component|
            component
            .and_then(|x| if x == "." || x == ".." || x == "" {
                    Err(AppError::DenormalizedPath)
                } else {
                    Ok(x)
                })
        );

    (path, path_trailing_slash)
}

async fn serve_req(req: Request<Body>, config: ApplicationConfiguration) -> Result<Response<Pin<Box<dyn hyper::body::HttpBody<Data=hyper::body::Bytes, Error=hyper::Error> + Send>>>, AppError> {
    let (mut path, path_trailing_slash) = request_path_to_segments(req.uri().path());

    // Skipping the topic of attrs for now as they're traditionally not considered in bare repos.
    // Consideration would need to be added later by manually interpreting .gitattributes files
    // found during walking.
    // Also, for submodule support, this would need to be checked ... when?
//     let attrs = repo.get_attr(path, "foo", git2::AttrCheckFlags::FILE_THEN_INDEX);

    // Tracks `repo` with a working directory to call `git annex` in. This is always "the git
    // repository" (in the sense of "the checkout that contains .git" in non-bare repos, and the
    // bare repo (being the only thing there is) in bare repos), because that is where git-annex
    // needs to be called from.
    let mut repopath = config.repopath.clone();
    let repo = open_repo(repopath.as_path())?;

    let head = repo.head()
        .map_err(|_| AppError::IncompleteGit("HEADless repository"))?
        .target()
        .ok_or_else(|| AppError::IncompleteGit("HEAD has no value"))?;
    let oid = repo.find_commit(head)
        .map_err(|_| AppError::IncompleteGit("No commit at HEAD"))?
        // Or maybe it's better to store the Object? Currently aiming for readability, hoping
        // performance does not suffer.
        .tree_id();
    let mut cursor = Cursor { repo, oid, mode: FileModeHint::Directory };
    let mut path_in_repo = Vec::new();

    while let Some(component) = path.next().transpose()? {
        // There is a new path component to follow, thus we assume we can walk whatever is there,
        // and all we can walk down is a tree -- if it's not a tree (or didn't become a tree after
        // symlink and submodule resolution in the last step), there is nothing below it.

        {
            let tree = cursor.repo.find_tree(cursor.oid)
                .map_err(|_| AppError::NotFound)?;

            let entry = tree.get_name(&component)
                .ok_or(AppError::NotFound)?;

            cursor.mode = (&entry).try_into()
                .map_err(|_| AppError::IncompleteGit("Unknown entry mode"))?;

            // Here would be the place to look at .gitattributes as well

            cursor.oid = entry.id();
        }

        path_in_repo.push(component);

        // FIXME following symlinks would go here

        // I'd be tempted to make this one of my "more of an if" while loops, but the fact that
        // we're hard-setting the cursor to mode=Directory on entering a new repository because
        // a commit's tree is a Tree and not a TreeEntry would make that pointless.
        if let FileModeHint::Gitlink = cursor.mode {
            // Creating a new cursor for the submodule
            //
            // Not changing cursor.repo -- that's a lie that later breaks the commit lookup.
            // We'd have to,
            // * look the path-so-far up in a configured list of submodules, or
            // * look the path-so-far up in .gitmodules by path, and come up with something from
            //   the url, or
            // * in a non-bare repo, we could also just open repo + path-so-far
            // cursor.repo = open_repo("/tmp/nonbarerepo2/submodules/some-submodule/")
            // cursor.repo = open_repo("/tmp/nonbarerepo2/.git/modules/submodules/some-submodule/")
            //     .map_err(|_| AppError::IncompleteGit("Error opening repository"))?;

            // FIXME: implement a find_submodule_by_path using iteration over submodules
            // Just joining the paths is OK here -- even if it's "../../etc", it will only open a
            // repo if that's already a submodule.
            if let Ok(Ok(sub)) = cursor.repo.find_submodule(&path_in_repo.join("/")).map(|x| x.open()) {
                cursor.repo = sub;
                repopath = std::path::PathBuf::from(cursor.repo.workdir()
                                .expect("A found submodule should be a non-bare repository on its own"));
            }
            // FIXME: Detect that none of the discovery paths worked rather than just sticking with
            // he curren one
            cursor.oid = cursor.repo.find_commit(cursor.oid)
                    .map_err(|_| AppError::IncompleteGit("Submodule commit not found in indicated repository"))?
                    .tree_id();
            cursor.mode = FileModeHint::Directory;
            path_in_repo.clear();
        }
    }

    let Cursor { repo, oid, mode } = cursor;

    if let FileModeHint::Symlink = mode {
        let reference = repo.find_object(oid, Some(git2::ObjectType::Blob))
            .map_err(|_| AppError::IncompleteGit("Symlink not backed by a blob object"))?
            .peel_to_blob()
            .map_err(|_| AppError::IncompleteGit("Symlink not backed by an actual blob"))?
            .content()
            // Cloned right away to not keep a git object open across an await point later on
            .to_vec();

        if reference.starts_with(b"/") {
            return Err(AppError::NotImplemented("No resolution rules for absolute symlinks"));
        }

        if let Some((relative, hash)) = gitannex::is_annex_symlink(&reference) {
            let repo_is_bare = repo.is_bare();
            drop(path); // Needs to be told explicitly as the compiler can't see through the `impl` result that produced the path that it can be NLL'd to free req
            return serve_annexed(req, config, &repopath, repo_is_bare, relative, hash).await;
        }

        if reference.starts_with(b"../") {
            return Err(AppError::NotImplemented("No resolution rules for upwards symlinks yet"));
        }

        return Err(AppError::NotImplemented("Not even resolution rules for plain symlinks yet"));
    }

    match (mode, path_trailing_slash) {
        (FileModeHint::Directory, false) => Err(AppError::NotFound), // or do what most servers do and redirect
        (FileModeHint::Directory, true) => {
            let tree = repo.find_tree(oid)
                .map_err(|_| AppError::IncompleteGit("Directory without tree content"))?;

            Ok(html_response(hyper::StatusCode::OK, html! {
                : horrorshow::helper::doctype::HTML;
                html {
                    head {
                        title : "Directory listing";
                    }
                    body {
                        ul {
                            @ for (link, label) in tree.iter().map(entry_to_href_and_name) {
                                li {
                                    a(href = link) {
                                        : label
                                    }
                                }
                            }
                        }
                    }
                }
            }))
        },
        (FileModeHint::File, true) => Err(AppError::NotFound),
        (FileModeHint::File, false) => {
            let blob = repo.find_blob(oid)
                .map_err(|_| AppError::IncompleteGit("Regular file without blob content"))?;
            // Not sending a media type -- we don't know anyway, the browser's guess is as good
            // as ours
            // FIXME allow asynchronous access (but libgit2 doesn't work that way)
            let body: Body = hyper::body::Bytes::copy_from_slice(blob.content()).into();
            let boxed: Pin<Box<dyn hyper::body::HttpBody<Data=hyper::body::Bytes, Error=hyper::Error> + Send>> = Box::pin(body);
            Ok(Response::new(boxed))
        },

        (FileModeHint::Symlink, _) | (FileModeHint::Gitlink, _) => unreachable!(
            "Weeded out during iteration or served as annex files in previous steps"
            ),
    }
}

async fn run_server(addr: SocketAddr, config: ApplicationConfiguration) {
    println!("Listening on http://{}, config {:?}", addr, config);

    let make_svc = make_service_fn(|_| {
        let config = config.clone();
        async {
            Ok::<_, Error>(service_fn(move |req| {
                let config = config.clone();
                async {
                    serve_req_errormapping(req, config).await
                }
            }))
        }
    });

    Server::bind(&addr)
        .serve(make_svc)
        .await
        .expect("Failed to start server");
}

fn main() {
    use structopt::StructOpt;
    let args = ApplicationConfiguration::from_args();

    let mut rt = Runtime::new().unwrap();
    rt.block_on(run_server(args.bind, args));
}
