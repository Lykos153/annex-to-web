This is an in-development web server that serves a (bare?) git-annex repository over HTTP.

It is currently in very early stages of development,
and it's not sure whether it's going anywhere.

The canonical web site of this project is
<https://gitlab.com/chrysn/annex-to-web>.

Planned features
----------------

This is a web server that is run on a
(typically but not necessarily bare)
git-annex repository, and serves its content.
(Non-bare repositories are supported,
but their index and directory contents are ignored).

Regular git files (blobs) and directories (trees)
are served directly or as a directory index, respectively.

Files under git-annex control are served when available,
and (depending on availability or configuration)
either obtained from other remotes and served on the fly,
redirect to other remotes that are known to have it and to run a similar server,
and worst case serve a "try making any of those remotes available" message.

Git submodules are served if there is a known (configured, or implied in non-bare repositories) clone usable.
Symlinks are followed within the git repository or available submodules,
and upwards to the including module when pointing above the current module.

Current status
--------------

Git repositories, both bare and non-bare, are served with files and directory listings,
as are git-annex files inside them.

Absent git-annex files are recognized,
but do not start a ``git annex get`` yet or redirect;
they do show "see other remotes" message with further links.

Locked git-annex files are completely disregarded.
(And how would I recognize them?)

Submodules are traversed, but so far only in checkouts
(as bare repositories do not contain submodules in the same way,
and will require additional configuration).

AOB
---

The project is written in Rust and can be run using ``cargo run "[::1]:3000" ./some/repository.git``
to serve on port 3000 of the loopback address.
It aims for asynchronous execution,
but is occasionally sloppy there,
hoping for the async ecosystem to provide easy solutions
before the sloppiness becomes a problem here.
(For example, ``git annex`` is called blockingly pending `an async-std Command implementation`_,
and git reads are just assumed to be served from cache).

.. _`an async-std Command implemenmtation`: https://github.com/async-rs/async-std/issues/22

Code was written by chrysn <chrysn@fsfe.org> and is published under the terms of the GNU AGPL 3.0 or any later version.
